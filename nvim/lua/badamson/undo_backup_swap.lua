---------------------------------------------------------------------------------------------------
-- undo settings
local os_name = vim.loop.os_uname().sysname
local USER = os_name == "Windows_NT" and os.getenv("USERNAME") or os.getenv("USER")

local base_dir
if os_name == "Windows_NT" then
  base_dir = os.getenv("USERPROFILE") .. "\\AppData\\Local\\nvim-data\\"
else
  base_dir = "/home/" .. USER .. "/.config/nvim/"
end

local SWAPDIR   = base_dir .. "swap/"
local BACKUPDIR = base_dir .. "backup/"
local UNDODIR   = base_dir .. "undo/"

if vim.fn.isdirectory(SWAPDIR) == 0 then
	vim.fn.mkdir(SWAPDIR, "p")
end

if vim.fn.isdirectory(BACKUPDIR) == 0 then
	vim.fn.mkdir(BACKUPDIR, "p")
end

if vim.fn.isdirectory(UNDODIR) == 0 then
	vim.fn.mkdir(UNDODIR, "p")
end

vim.opt.backupdir = BACKUPDIR
vim.opt.directory = SWAPDIR
vim.opt.undodir   = UNDODIR

vim.opt.swapfile = true
vim.opt.backup   = true
vim.opt.undofile = true
