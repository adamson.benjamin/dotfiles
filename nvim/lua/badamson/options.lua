vim.wo.number = true

vim.opt.tabstop    = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab  = true
vim.opt.wrap       = false
vim.bo.softtabstop = 2

---------------------------------------------------------------------------------------------------
-- timeout between mapped keypresses
vim.opt.timeoutlen = 200

---------------------------------------------------------------------------------------------------
-- highlight trailing whitespace
vim.cmd('highlight EoLSpace ctermbg=238 guibg=#FF0000')
vim.cmd('match EoLSpace /\\s\\+$/')

---------------------------------------------------------------------------------------------------
-- format options
-- Disable the behavior of automatically continuing comments on the next line
-- when hitting 'o' or 'O'.
--
-- This ensures comments don't automatically continue when moving to a new line.
vim.cmd('autocmd BufEnter * set formatoptions-=cro')
