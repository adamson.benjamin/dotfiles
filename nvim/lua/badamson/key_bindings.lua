-- simplify my daily life
local map = vim.keymap.set

map('n', ';', ':')

map('x', 'p', 'pgvy', { noremap = true })

-- map new escape in insert and visual modes
map('n', ';w', '<ESC>:w<CR>')
map('n', ';w', '<ESC>:w<CR>')
map('n', ';x', '<ESC>:x<CR>')
map('n', ';q', '<ESC>:q<CR>')

-- These two remap the '`' and the escape keys to refer to one another.
map('i', '`', '<ESC>')
map('i', '<ESC>', '`')

-- Helper function that splits the window and makes it active
function my_split(cmd)
  vim.cmd(cmd)
  local win = vim.api.nvim_get_current_win()
  local buf = vim.api.nvim_create_buf(true, true)
  vim.api.nvim_win_set_buf(win, buf)
end

-- Some key-bindings for navigating between [up, down, left, right] between split-panes
map('n', '<A-h>', function () my_split(':vsplit'); end)
map('n', '<C-h>', '<C-w>h')

map('n', '<A-l>', function () my_split(':botright split'); end)
map('n', '<C-l>', '<C-w>h')

map('n', '<A-h>', function () my_split('vsplit'); end)
map('n', '<C-h>', '<C-w>h')

map('n', '<A-l>', function () my_split(':botright vsplit'); end)
map('n', '<C-l>', '<C-w>l')

map('n', '<A-j>', function () my_split(':botright split'); end)
map('n', '<C-j>', '<C-w>j')

map('n', '<A-k>', function () my_split(':topleft split'); end)
map('n', '<C-k>', '<C-w>k')

map('n', '<A-v>', vim.cmd.Ex)

---------------------------------------------------------------------------------------------------
-- ctags
map('n', '<C-j>', '<C-]>')
map('n', '<C-u>', '<C-T>')

---------------------------------------------------------------------------------------------------
-- FZF
map("n", '<C-p>', require("fzf-lua").files)
map("n", '<C-o>', require("fzf-lua").oldfiles)
