return {
  {
    "nvim-lualine/lualine.nvim",
    dependencies = { 'nvim-tree/nvim-web-devicons' },

    config = function()
      require("lualine").setup({
        options = { icons_enabled = false },
        sections = {
          lualine_a = {'mode'},
          lualine_b = {},
          lualine_c = {'filename'},
          lualine_x = {'encoding', 'fileformat'},
          lualine_y = {'progress'},
          lualine_z = {'location'}
        },
      })
    end
  }
}
