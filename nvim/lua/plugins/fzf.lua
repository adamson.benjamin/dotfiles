return {
  "ibhagwan/fzf-lua",
  -- optional for icon support
  dependencies = { "nvim-tree/nvim-web-devicons" },

  config = function()
    local actions = require "fzf-lua.actions"
    require("fzf-lua").setup({
      defaults = {
        git_icons   = false,
        file_icons  = false,
        color_icons = false,
      },
      actions = {
        files = {
          false,
          ["enter"]  = actions.file_edit_or_qf,
          ["ctrl-o"] = actions.file_vsplit,
        },
      },
    })
  end
}
