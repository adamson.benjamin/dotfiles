require("badamson.leader")
require("badamson.lazy")
require("badamson.options")
require("badamson.key_bindings")
require("badamson.undo_backup_swap")

require("lspconfig").clangd.setup{}

local cmp = require('cmp')

cmp.setup({
  sources = {
    {name = 'nvim_lsp'},
  },
  snippet = {
    expand = function(args)
      vim.snippet.expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert({}),
})

vim.cmd("colorscheme carbonfox")
