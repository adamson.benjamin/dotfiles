#!/bin/sh
INSTALL_DIR="$HOME/gcc-out"

# NOTE: This was necessary on debian..
sudo apt-get install flex
git clone git://gcc.gnu.org/git/gcc.git
pushd gcc
./contrib/download_prerequisites
popd

mkdir objdir
cd objdir
../gcc/configure --prefix="$INSTALL_DIR"  \
                 --enable-languages=c,c++ \
                 --enable-shared          \
                 --enable-threads=posix   \
                 --enable-__cxa_atexit    \
                 --enable-clocale=gnu     \
                 # LEAVE THIS BLANK
make -j8
