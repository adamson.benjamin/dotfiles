#!/bin/sh

export PATH=.:$PATH

#alias bb="scripts/build.bash"
alias be="scripts/editor.bash"
alias br="scripts/run.bash"
alias bbf='scripts/format-code.bash'
alias bbt='scripts/run-tests.bash'
alias bbr='bb;br'
alias cbb='clear;bb'
