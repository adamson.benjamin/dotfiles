#!/bin/sh

alias u="cd ../"
alias uu="u;u"
alias uuu="uu;u"
alias uuuu="uuu;u"
alias uuuuu="uuuu;u"
alias uuuuuu="uuuuu;u"
alias uuuuuuu="uuuuuu;u"
