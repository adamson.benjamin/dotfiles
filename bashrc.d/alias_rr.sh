#!/bin/sh

function recursive_find_replace() {
  ARG1="$1"
  ARG2="$2"

  echo "-- replacing $ARG1 with $ARG2"
  \grep -rl $ARG1 . --exclude-dir=.git | xargs sed -i "s/$ARG1/$ARG2/g" &>/dev/null
  if [[ $? -eq 123 ]]; then
    echo "-- No occurrences of $ARG1 found to replace"
  else
    echo "-- finished"
  fi
}
alias rr="recursive_find_replace $@"
