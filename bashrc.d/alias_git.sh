#!/bin/sh

alias gc="git commit"
alias gca="git commit --amend"
alias gd="git diff"
alias gdc="git diff --cached"
alias ga="git add"
alias gaa="git add ."
alias gf="git fetch"
alias gl="git lola"
alias gs="git status"
alias gp="git push"

alias gg="git grep"
