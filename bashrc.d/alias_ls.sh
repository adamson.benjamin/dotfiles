#!/bin/sh

alias ls="eza --header -l --group-directories-first --total-size --time-style relative"
alias la="ls -la"
alias lt="ls -T"
