#!/bin/sh

COLOR1="\[\e[38;5;33m\]"    # Bright Blue
COLOR2="\[\e[38;5;46m\]"    # Bright Green
WHITE="\[\e[38;5;15m\]"     # White
export PS1="[${COLOR1}\u${WHITE}@${COLOR2}\h${WHITE}/\w]%$ "
