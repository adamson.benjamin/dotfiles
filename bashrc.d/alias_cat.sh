#!/bin/sh

if command -v batcat &> /dev/null; then
  alias cat='batcat --paging=never --style=plain,header'
elif command -v bat &> /dev/null; then
  alias cat='bat --paging=never --style=plain,header'
fi
