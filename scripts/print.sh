#!/usr/bin/env bash

####################################################################################################
# Functions for printing to the terminal using various colors.
####################################################################################################

# Helper function used internally to print to the terminal with a specific color.
function print_color() {
  tput setaf $1;
  printf '%s\n' "$2"
  tput sgr0;
}

function print_red() {
  print_color 1 "$1"
}

function print_green() {
  print_color 2 "$1"
}

function print_yellow() {
  print_color 3 "$1"
}

function print_white() {
  print_color 7 "$1"
}

function print_blue() {
  print_color 4 "$1"
}

function print_magenta() {
  print_color 5 "$1"
}

function print_cyan() {
  print_color 6 "$1"
}

function print_procedure() {
  if [ 3 -gt $# ]; then
    print_red "print_procedure expects '3' arguments , actual: '$#'"
    return
  fi

  local color="$1"
  local description="$2"
  shift 2
  local cmd=("$@")
  print_color "$color" ">> $description - Starting .."
  "${cmd[@]}"  # Execute command array
  print_color "$color" "<< '$description' - Finished !!"
}

function print_procedure_white() {
  print_procedure 7 "$@"
}

function print_procedure_red() {
  print_procedure 1 "$@"
}

function print_procedure_green() {
  print_procedure 2 "$@"
}

function print_procedure_yellow() {
  print_procedure 3 "$@"
}

function print_procedure_blue() {
  print_procedure 4 "$@"
}

function print_procedure_magenta() {
  print_procedure 5 "$@"
}

function print_procedure_cyan() {
  print_procedure 6 "$@"
}

function print_procedure_if() {
  if [ 4 -gt $# ]; then
    print_red "print_procedure_if expects '4' arguments , actual: '$#'"
    return
  fi
  if [ "$2" = true ]; then
    $1 "$3" $4
  fi
}

function print_procedure_white_if() {
  print_procedure_if print_procedure_white "$@"
}

function print_procedure_red_if() {
  print_procedure_if print_procedure_red "$@"
}

function print_procedure_green_if() {
  print_procedure_if print_procedure_green "$@"
}

function print_procedure_blue_if() {
  print_procedure_if print_procedure_blue "$@"
}

function print_procedure_magenta_if() {
  print_procedure_if print_procedure_magenta "$@"
}

function print_procedure_cyan_if {
  print_procedure_if print_procedure_cyan "$@"
}
