#!/bin/bash
source "scripts/print.sh"

###################################################################################################
# FUNCTION DEFINITIONS
###################################################################################################
function install_system_software() {
  if command -v apt-get &> /dev/null; then
    INSTALLER="apt-get -y install"
  elif command -v dnf &> /dev/null; then
    INSTALLER="dnf -y install"
  elif command -v yum &> /dev/null; then
    INSTALLER="yum -y install"
  elif command -v pacman &> /dev/null; then
    INSTALLER="pacman -S --noconfirm"
  elif command -v zypper &> /dev/null; then
    INSTALLER="zypper -y install"
  else
    echo "No supported package manager found."
    exit 1
  fi

  sudo $INSTALLER            \
        curl                 \
        git                  \
        neovim               \
        bat                  \
        eza                  \
        ripgrep              \
        stow                 \
        fzf                  \
        # LEAVE THIS BLANK
}

prompt_to_install() {
  program="$1"
  install_fn="$2"

  print_yellow "Do you wish to install $1? (Press 1 or 2)"
  select yn in "Yes" "No"; do
      case $yn in
    Yes ) $install_fn; break;;
    No ) break;;
      esac
  done
}

check_bashrc() {
  if [ ! -f ~/.bashrc ] && [ -f /etc/skel/.bashrc ]; then
    cp /etc/skel/.bashrc ~/.bashrc
  elif [ ! -f ~/.profile ] && [ -f /etc/skel/.profile ]; then
    cp /etc/skel/.profile ~/.bashrc
  fi

  MARKER="ONETIME_MARKER_STRING"
  if ! grep -qF "$MARKER" ~/.bashrc; then
    # Append your text to .bashrc
    cat >> ~/.bashrc <<EOF

# ONETIME_MARKER_STRING
PROMPT_COMMAND='history -a'

for file in ~/.bashrc.d/*.sh;
do
  source "\$file"
done
EOF
  fi
}

check_bashprofile() {
  if [ ! -f ~/.bash_profile ]; then
    cat >> ~/.bash_profile <<EOF
# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
  . ~/.bashrc
fi
EOF
  fi
}

setup_stows() {
  mkdir -p $(realpath ~/.bashrc.d)
  mkdir -p $(realpath ~/.config/ghostty)
  stow --target=$(realpath ~/.bashrc.d)       --adopt bashrc.d
  stow --target=$(realpath ~/.config/ghostty) --adopt ghostty
  stow --target=$(realpath ~)                 --adopt config
}

copy_nvim_config() {
  rm -rf ~/.cache/nvim
  rm -rf ~/.config/nvim
  rm -rf ~/.local/share/nvim
  rm -rf ~/.local/state/nvim

  cp -rf nvim ~/.config/
  mkdir -p ~/.local/share/nvim/ctags
}

###################################################################################################
# MAIN:
print_procedure_cyan "System Packages" install_system_software
print_procedure_cyan "Checking bashrc" check_bashrc
print_procedure_cyan "Checking profile" check_bashprofile
print_procedure_cyan "Setting up stows" setup_stows
print_procedure_cyan "Copying nvim config" copy_nvim_config
